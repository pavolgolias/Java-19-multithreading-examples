package sk.golias.demo4;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

// if we would have synchronized used on methods "stageOne" and "stageTwo", it uses unique lock for Worker object, so at the same time
// only one of "stageOne" and "stageTwo" method can run, the second one have to wait.
// using synchronized blocks enables to run method simultaneously, because it requires "lock1" and "lock2"
public class Worker {
    private final Object lock1 = new Object();
    private final Object lock2 = new Object();

    private Random random = new Random();

    private List<Integer> list1 = new ArrayList<>();
    private List<Integer> list2 = new ArrayList<>();

    public void stageOne() {
        synchronized ( lock1 ) {
            try {
                Thread.sleep(1);
            }
            catch ( InterruptedException e ) {
                e.printStackTrace();
            }

            list1.add(random.nextInt(100));
        }
    }

    public synchronized void stageTwo() {
        synchronized ( lock2 ) {
            try {
                Thread.sleep(1);
            }
            catch ( InterruptedException e ) {
                e.printStackTrace();
            }

            list2.add(random.nextInt(100));
        }
    }

    public void process() {
        for ( int i = 0; i < 1000; i++ ) {
            stageOne();
            stageTwo();
        }
    }

    public void main() throws InterruptedException {
        System.out.println("Starting...");

        long start = System.currentTimeMillis();
        Thread t1 = new Thread(this::process);
        t1.start();

        Thread t2 = new Thread(this::process);
        t2.start();

        t1.join();
        t2.join();
        long end = System.currentTimeMillis();

        System.out.println("Time take: " + (end - start));
        System.out.println("List1: " + list1.size() + " List2: " + list2.size());
    }
}
